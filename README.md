# LANCEMENT DU JEU

Lancer index.html directement sur le navigateur, ou cliquer su "exécuter" depuis l'IDE

# EXPOSER LE JEU PUBLIQUEMENT (NGROK)

http ngrok [PORT]

# JOUER

- Appuyer sur le deck (ou touche d) pour tirer des cartes; utiliser les différentes fonctionnalités visibles à l'écran
- Tester la connectivité depuis l'inspecteur du navigateur

# AUTEURS

Projet réalisé par ACHBOUQ Slimane et ALAA HAZIM Zaid