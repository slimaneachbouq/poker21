import { API_URL } from './config.js';
import { enableReq, preventReq } from './controller.js';

let controller;

export const getJson = async function (endpoint) {
    try {
        preventReq();

        controller = new AbortController();
        const signal = controller.signal;

        //const res = await Promise.race([fetch(API_URL + endpoint), timeout(1)]);
        document.getElementById('abort-btn').style.display = '';
        const res = await fetch(API_URL + endpoint, {
          signal: signal
        });
        
        if (!res.ok) {
            let message = res.status === 404 ? 'Api url not correct !' : 'Error occured while fetching data from API';
            throw new Error(`${message} (${res.status})`)
        }
        const data = await res.json();
        enableReq();
        document.getElementById('abort-btn').style.display = 'none';
        
        return data;
    } catch (error) {
        enableReq();
        document.getElementById('abort-btn').style.display = 'none';
        throw error;
    }
}

export const abortReq = function () {
  console.log('abort !');
  controller.abort();
}

export const vibrate = function () {
  if (!window) {
      return;
  }

  if (!window.navigator) {
      return;
  }

  if (!window.navigator.vibrate) {
      return;
  }

  window.navigator.vibrate(100);
}

const timeout = function (s) {
    return new Promise(function (_, reject) {
      setTimeout(function () {
        reject(new Error(`Request took too long! Timeout after ${s} second`));
      }, s * 1000);
    });
  };