import { getJson } from './helper.js';

const _specialCardsValues = {
    'JACK': 10,
    'ACE': 0,
    'KING': 10,
    'QUEEN': 10
};

export let state = {
    username: null,
    deck_id: null,
    score: 0,
    remaining: 0,
    drawed_card: {},
    game_over: false
};

export const initState = function (username) {
        state = {
            username: username,
            deck_id: null,
            score: 0,
            remaining: 0,
            drawed_card: {},
            table_cards: [],
            game_over: false
        };
}

export const initStateWithLocalStorage = function (localStorageState) {
    state = localStorageState;
}

const _updateScore = function (cardValue) {
    state.score += _specialCardsValues.hasOwnProperty(cardValue) ? _specialCardsValues[cardValue] : parseInt(cardValue);
}

export const createNewDeck = async function () {
    try {
        const data = await getJson('/api/deck/new/shuffle/?deck_count=1');
        const deck_id = data.deck_id;
        state.deck_id = deck_id;
        state.remaining = data.remaining;

        localStorage.setItem('poker21', JSON.stringify(state));
    } catch (error) {
        throw error;
    }
}

export const drawCard = async function (deck_id) {
    try {
        const data = await getJson('/api/deck/'+deck_id+'/draw/?count=1');
        state.drawed_card = data.cards[0];
        _updateScore(data.cards[0].value);
        state.remaining = data.remaining;
        state.table_cards.push(data.cards[0]);

        localStorage.setItem('poker21', JSON.stringify(state));
    } catch (error) {
        throw error;
    }
}