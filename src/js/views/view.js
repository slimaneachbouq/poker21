export default class View {
    _remainingScore = document.getElementById('remaining');
    _score = document.getElementById('score');
    _modal = document.querySelector('.modal');
    _modal_overlay = document.querySelector('.overlay');
    _modal_title = document.getElementById('modal-title');
    _modal_message = document.getElementById('modal-message');
    _modal_img = document.querySelector('.modal-img');
    _close_modal_btn = document.querySelector('.close-modal');

    _close_modal() {
        this._modal.classList.add('hidden');
        this._modal_overlay.classList.add('hidden');
    }

    updateElement(el, value) {
        el.innerHTML = value;
    }

    _initModal() {
        this._modal_title.innerHTML = '';
        this._modal_img.src = '';
        this._modal_message.innerHTML = '';
    }

    displayModal(title, message, image) {
        this._initModal();
        this._modal_title.innerHTML = title;
        if (image) {
            this._modal_img.src = `/src/img/${image}`;
        } else {
            this._modal_message.innerHTML = message;
        }

        this._modal.classList.remove('hidden');
        this._modal_overlay.classList.remove('hidden');
    }

    addCloseModalHandler() {
        this._close_modal_btn.addEventListener('click', () => {
            this._close_modal();
        });
        this._modal_overlay.addEventListener('click', () => {
            this._close_modal();
        });
    }
}