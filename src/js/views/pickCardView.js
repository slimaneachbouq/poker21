import View from './view.js';

class PickCardView extends View {
    _pickedCardTranslateX = -115;
    _pickElementBtn = document.querySelector('.deck_cards_section');

    _pickedCard = document.getElementById('picked_card');
    _newGameBtn = document.querySelector('.btn--new');
    _target = document.getElementsByClassName('table')
    _card = document.getElementById('test_card');
    _holdBtn = document.querySelector('.btn--hold');
    _abortBtn = document.getElementById('abort-btn');

    _getOffset(el) {
        const rect = el.getBoundingClientRect();
        return {
          left: rect.left + window.scrollX,
          top: rect.top + window.scrollY
        };
    }

    renderCardImage(imgUrl, animate) {
        if (typeof animate === 'undefined') {
            this._card.animate([
                { transform: 'translate(-50%, -20%)' },
    
            ], {
                duration: 140,
                iterations: 1
            });
    
            setTimeout(() => {
                document.querySelector('.table').insertAdjacentHTML('beforeend', `<img src="${imgUrl}" alt="cards" class="cards" style="transform: translateX(${this._pickedCardTranslateX}px);"/>`);
                document.getElementById("placesound").play();
    
            }, 100);
        } else {
            document.querySelector('.table').insertAdjacentHTML('beforeend', `<img src="${imgUrl}" alt="cards" class="cards" style="transform: translateX(${this._pickedCardTranslateX}px);"/>`);
        }


        this._pickedCardTranslateX += 15;
    }

    addPickCardHandler(handler) {
        this._pickElementBtn.onclick = handler;
    }

    removePickCardEvent(abortReqHandler) {
        this._pickElementBtn.onclick = '';
        document.onkeypress = function (event) {
            const touche = event.key;

            if (touche === 'c' || touche === 'C') {
                abortReqHandler();
            }
        };
    }

    addHoldCardHandler(handler) {
        this._holdBtn.onclick = handler;
    }

    addAbortHandler(handler) {
        this._abortBtn.onclick = handler;
    }

    addKeypressHandler(pickCardHandler, abortReqHandler) {
            document.onkeypress = function (event) {
                const touche = event.key;
    
                if (touche === 'd' || touche === 'D') {
                    pickCardHandler();
                }

                if (touche === 'c' || touche === 'C') {
                    abortReqHandler();
                }
            };
    }

    removeHoldCardEvent() {
        this._holdBtn.onclick = '';
    }

    addNewGameHandler(handler) {
        this._newGameBtn.onclick = handler;
    }

    removeNewGameHandler() {
        this._newGameBtn.onclick = '';
    }
}

export default new PickCardView();