import { state, createNewDeck, drawCard, initState, initStateWithLocalStorage} from './model.js';
import view from './views/view.js';
import PickCardView from './views/pickCardView.js';
import { abortReq, vibrate } from './helper.js';

const drawCardController = async function(isHold) {
    try {
        await drawCard(state.deck_id);
        PickCardView.renderCardImage(state.drawed_card.image);
        PickCardView.updateElement(PickCardView._remainingScore, state.remaining);
        PickCardView.updateElement(PickCardView._score, state.score);
        if (state.remaining < 52) {
            PickCardView.addPickCardHandler(drawCardController.bind(this, false));
        }
        enableHoldBtn();
        checkGameOver(isHold);
    } catch (error) {
        PickCardView.displayModal('There is an error !  💥💥', error.message);
    }
}

const enableHoldBtn = async function() {
    if (state.remaining <= 51) {
        PickCardView.addHoldCardHandler(drawCardController.bind(this, true));
    }
}

const checkGameOver = async function(isHold) {
    state.game_over = (state.score > 21 || state.score === 21) || isHold;

    if((isHold && state.score > 21) || state.score === 21) {
        PickCardView.displayModal('Vous avez gagné !', '', 'bravo.jpg');
        vibrate();
    } else if ((isHold && state.score <= 21) || state.score > 21) {
        PickCardView.displayModal('Vous avez perdu !', '', 'game-over.jpg');
        vibrate();
    }

    if (state.remaining < 52) {
        PickCardView.addNewGameHandler(newGame);
    }

    if(state.game_over) {
        PickCardView.removePickCardEvent(abortReq);
        PickCardView.removeHoldCardEvent();
        localStorage.removeItem("poker21");
    }
}

const newGame = async function() {
    init(state.username);
    PickCardView.updateElement(PickCardView._remainingScore, state.remaining);
    PickCardView.updateElement(PickCardView._score, state.score);
    PickCardView.updateElement(PickCardView._pickedCard, '');
}

const init = async function (username) {
    try {
        checkInternetConnectionStatus();
        PickCardView.removeHoldCardEvent();
        PickCardView._pickedCardTranslateX = -115;
        initState(username);
        document.getElementById('username-value').innerText = state.username;
        PickCardView.addCloseModalHandler();
        await createNewDeck();
        PickCardView.updateElement(PickCardView._remainingScore, state.remaining);
        PickCardView.addKeypressHandler(drawCardController.bind(this, false), abortReq);
        PickCardView.removeHoldCardEvent();
        PickCardView.removeNewGameHandler();
    } catch (error) {
        PickCardView.displayModal('There is an error !  💥💥', error.message);
    }
}

const checkInternetConnectionStatus = function () {
    let ifConnected = window.navigator.onLine;

    if (ifConnected) {
        document.getElementById('connection-status').style.color = '#2eb35a';
        document.querySelector('#connection-status > span').innerText = 'ONLINE';
    } else {
        document.getElementById('connection-status').style.color = '#d22c32';
        document.querySelector('#connection-status > span').innerText = 'OFFLINE';
    }
    
    window.addEventListener('online', function(){
        document.getElementById('connection-status').style.color = '#2eb35a';
        document.querySelector('#connection-status > span').innerText = 'ONLINE';
        }, false);
      
    window.addEventListener('offline', function(){
        document.getElementById('connection-status').style.color = '#d22c32';
        document.querySelector('#connection-status > span').innerText = 'OFFLINE';
        }, false);
}

export const enableReq = function() {
    enableHoldBtn();
    PickCardView.addPickCardHandler(drawCardController.bind(this, false));
    PickCardView.addKeypressHandler(drawCardController.bind(this, false), abortReq);
}

export const preventReq = function() {
    PickCardView.removePickCardEvent(abortReq);
    PickCardView.removeHoldCardEvent();
}

document.getElementById('play-btn').addEventListener('click', function(e) {
    e.preventDefault();

    let username = document.getElementById('username').value;
    document.querySelector('.game-table').style.display = '';
    document.querySelector('.deck_cards_section').style.display = '';
    document.querySelector('.game-login').style.display = 'none';
    init(username);
})

PickCardView.addAbortHandler(abortReq);
document.getElementById('abort-btn').style.display = 'none';

if ("poker21" in localStorage) {
    try {
        document.querySelector('.game-table').style.display = '';
        document.querySelector('.deck_cards_section').style.display = '';
        document.querySelector('.game-login').style.display = 'none';

        checkInternetConnectionStatus();
        initStateWithLocalStorage(JSON.parse(localStorage.getItem('poker21')));
        document.getElementById('username-value').innerText = state.username;
        if (state.remaining < 52) {
            PickCardView.addNewGameHandler(newGame);
        }
       
        PickCardView.addCloseModalHandler();
        PickCardView.updateElement(PickCardView._remainingScore, state.remaining);
        PickCardView.updateElement(PickCardView._score, state.score);
        PickCardView.addPickCardHandler(drawCardController.bind(this, false));
        PickCardView.addKeypressHandler(drawCardController.bind(this, false), abortReq);
        enableHoldBtn();
        if (state.remaining < 52) {
            PickCardView.addPickCardHandler(drawCardController.bind(this, false));
        }

        state.table_cards.forEach((item, index) => {
            PickCardView.renderCardImage(item.image, false);
        });
        
    } catch (error) {
        PickCardView.displayModal('There is an error !  💥💥', error.message);
    }
}



